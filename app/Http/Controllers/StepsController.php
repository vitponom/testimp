<?php

namespace App\Http\Controllers;

use App\Article;
use App\User;
use Illuminate\Http\Request;

class StepsController extends Controller
{
    public function two(){
        $articles = Article::all();
        return view('second.index', compact('articles'));
    }

    public function three(){
        return view('third.index');
    }

    public function four(){
        $articles = Article::all();
        return view('four.index', compact('articles'));
    }

    public function five(){
        $articles = Article::all();
        $users = User::all();
        return view('five.index', compact('articles', 'users'));
    }

    public function six(){
        $user = User::findOrFail(2);
        return view('six.index',compact('user'));
    }
}

<?php

use Illuminate\Database\Seeder;
use App\Article;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i = 0; $i < 10; $i++){
            Article::create([
               'text' =>
                '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc imperdiet ultricies purus quis pharetra. Sed suscipit mi fermentum odio
                     imperdiet facilisis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec et orci id mi
                     rhoncus porta. '.$i.' 
                </p>'
            ]);
        }
    }
}

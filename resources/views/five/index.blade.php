@extends('layout.layout')
@section('content')
    <table class="table table-responsive table-striped">
        <thead>
            <tr>
                <td>Номер</td>
                <td>Статья</td>
                <td>Авторы</td>
            </tr>
        </thead>
        <tbody>
            @foreach($articles as $article)
                <tr>
                    <td>{!! $article->id !!}</td>
                    <td>{!! Str::limit($article->text, 25, '...') !!}</td>
                    <td>
                        <ul style="list-style: none;">
                       @foreach($users as $user)
                           <li>{!! $user->name !!} <span class="pull-right">{!! $article->isAuthor($user) !!}</span> </li>
                       @endforeach
                        </ul>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <p>Хотя можно проверять коллекция ли пришла</p>
@endsection

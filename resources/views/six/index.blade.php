@extends('layout.layout')
@section('content')
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <td>№</td>
            <th>Имя</th>
            <th>Опыт</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>{!! $user->id !!}</td>
            <td>{!! $user->name !!}</td>
            <td class="expire">{!! $user->experience !!}</td>
        </tr>
        </tbody>
    </table>
@endsection
@section('script')
    <script type="text/javascript">
        function expire(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{route('expire')}}',
                method: 'POST',
                data: { _token: "{!! csrf_token() !!}"},
                success: function(res){
                    $('td.expire').text(res);
                    console.log(res)
                },
            });
        }
        setInterval(() => expire(), 2000);
    </script>
@endsection
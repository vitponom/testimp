<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Test',
            'email' => 'test@test.com',
            'password' => Hash::make('000000')
        ]);
        User::create([
            'name' => 'second',
            'email' => 'second@test.com',
            'password' => Hash::make('111111')
        ]);
        User::create([
            'name' => 'third',
            'email' => 'third@test.com',
            'password' => Hash::make('222222')
        ]);
    }
}

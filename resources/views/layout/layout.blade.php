<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <meta name="description" content="Тестовое задание пользователи и статьи" />
    <meta name="keywords" content="Тестовое задание" />
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{!! asset('css/app.css') !!}" />
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        @yield('content')
        <hr/>
        <div class="links">
            <a href="{!! route('index') !!}">Главная</a>
            <a href="{!! route('stepTwo') !!}">Вводная</a>
            <a href="{!! route('stepThree') !!}">Работа с формами</a>
            <a href="{!! route('stepFour') !!}">Миграции</a>
            <a href="{!! route('stepFive') !!}">Типизация</a>
            <a href="{!! route('stepSix') !!}">ORM</a>
        </div>
    </div>
</div>
<script type="text/javascript" src="{!! asset('js/app.js') !!}"></script>
@yield('script')
</body>
</html>
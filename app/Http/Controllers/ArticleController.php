<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->user()->detach();
        $article->delete();
        return redirect()->back();
    }
}

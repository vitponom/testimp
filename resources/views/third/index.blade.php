@extends('layout.layout')
@section('content')
    {!! Form::open(array('route' => 'user.create','method'=>'POST', 'files' => true, 'class' => 'form-horizontal')) !!}
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Никнейм:</strong>
            {!! Form::text('nick', null, array('class' => 'form-control', 'placeholder' => 'Введите никнейм')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Имя:</strong>
            {!! Form::text('name', null, array('class' => 'form-control', 'placeholder' => 'Ваше имя')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Имя:</strong>
            {!! Form::text('surname', null, array('class' => 'form-control', 'placeholder' => 'Ваша фамилия')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Аватар:</strong>
            <label class="btn btn-default">
                {!! Form::file('avatar', array('class' => 'image')) !!}
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Телефон:</strong>
            {!! Form::text('phone', null, array('class' => 'form-control', 'placeholder' => 'Ваш телефон')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Пол:</strong>
            {!! Form::radio('sex', 'Мужской' , true) !!} <span>Мужской</span>
            {!! Form::radio('sex', 'Женский' , false) !!} <span>Женский</span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Соглашение о рассылке:</strong>
            <label class="btn btn-default">
            {!! Form::checkbox('success', true, true, array('class' => 'agree')) !!}
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Создать</button>
    </div>
    {!! Form::close() !!}
@endsection
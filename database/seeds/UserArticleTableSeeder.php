<?php

use Illuminate\Database\Seeder;
use App\User;

class UserArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = User::all();
        foreach ($users as $user){
            $user->article()->attach(rand(1,10));
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('surname',70)->nullable();
            $table->string('avatar', 140)->nullable();
            $table->string('phone',12)->nullable();
            $table->enum('sex',['Мужской','Женский'])->nullable();
            $table->string('nick', 100)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('surname');
            $table->dropColumn('avatar');
            $table->dropColumn('phone');
            $table->dropColumn('sex');
            $table->dropColumn('nick');
        });
    }
}

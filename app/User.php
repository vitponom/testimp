<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
        'surname', 'avatar', 'phone', 'sex', 'nick', 'experience'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function article(){
        return $this->belongsToMany(Article::class, 'users_articles', 'user_id','article_id');
    }

    public static function generate($lenght = 8){
        $chars = 'abcdefghijklmnopqrstuvwxyz';
        $charsLenght = strlen($chars);
        $result = '';
        for ($i = 0; $i < $lenght; $i++) {
            $result .= $chars[rand(0, $charsLenght - 1)];
        }
        return $result;
    }

    public static function generateEmail(){
        return User::generate().'@test.com';
    }
}

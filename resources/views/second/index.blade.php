@extends('layout.layout')
@section('content')
<table class="table table-responsive table-striped">
    <thead>
    <tr>
        <th>№</th>
        <th>Статья</th>
        <th>Авторы</th>
    </tr>
    </thead>
    <tbody>
        @foreach($articles as $article)
        <tr>
            <td>{!! $article->id !!}</td>
            <td>{!! Str::limit($article->text, 25, '...') !!}</td>
            <td>
                @foreach($article->user as $user)
                    <span class="user-article">{!! $user->name !!}</span>
                @endforeach
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
@endsection
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'articles';
    protected $fillable = ['text'];

    public function user(){
        return $this->belongsToMany(User::class, 'users_articles', 'article_id','user_id');
    }

    public function isAuthor($user){
        if(empty($user->id)){
            return "Передан не пользователь";
        } else {
            foreach ($this->user as $item) {
                if ($item->id == $user->id) {
                    return "Автор";
                } else {
                    return "Не Автор";
                }
            }
        }
    }
}

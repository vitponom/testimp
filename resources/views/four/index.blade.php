@extends('layout.layout')
@section('content')
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <td>№</td>
            <td>Статья</td>
            <td>Управление</td>
        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
            <tr>
                <td>{!! $article->id !!}</td>
                <td>{!! Str::limit($article->text, 25, '...') !!}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE','route' => ['article.destroy', $article->id], 'style'=>'display:inline']) !!}
                    {!! Form::button('Удалить', ['type' => 'submit', 'class' => 'btn btn-default']) !!}
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <pre>
        Пункт 2 задачи 4:
        <b>При этом сделать возможность обновить систему со старой версии (из первого задания) на новую без полного сброса бд </b>
        <br/>
        Добавлена миграция для расширения: 2019_10_23_135021_add_fields_to_users_table
        <br/>
        Добавляется с помощью php artisan make:migration ...
    </pre>
@endsection
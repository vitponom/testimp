<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');
Route::get('two', ['as' => 'stepTwo','uses' => 'StepsController@two']);
Route::get('three', ['as' => 'stepThree', 'uses' => 'StepsController@three']);
Route::get('four', ['as' => 'stepFour', 'uses' => 'StepsController@four']);
Route::get('five', ['as' => 'stepFive', 'uses' => 'StepsController@five']);
Route::get('six', ['as' => 'stepSix', 'uses' => 'StepsController@six']);
Route::get('three/create/{id}', ['as' => 'user.show', 'uses' => 'UserController@show']);
Route::post('three/create', ['as' => 'user.create', 'uses' => 'UserController@create']);
Route::post('six', ['as' => 'expire', 'uses' => 'UserController@postExpire']);
Route::resource('article',  'ArticleController')->only(['destroy']);

<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function create(Request $request){
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'success' => 'required',
            'phone' => 'required|numeric'
        ],[
            'name.required' => 'Не введено имя пользователя',
            'success.required' => 'Вы не дали согласия на получение рассылки',
            'phone.required' => 'Необходимо указать контактный телефон',
            'phone.numeric' => 'Для телефона возможны только цифры'
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput($request->all())->withErrors($validator->errors());
        }
        $input['avatar'] = '';
        $email = User::generateEmail();
        $password = User::generate();
        if($request->hasFile('avatar')){
            $image = $request->file('avatar');
            if(isset($image)) {
                $input['avatar'] = $image->hashName();
                $image->move(public_path('image'), $input['avatar']);
            }
        }
        $user = User::create([
            'name' => $request->input('name'),
            'email' => $email,
            'password' => $password,
            'nick' => $request->input('nick'),
            'surname' => $request->input('surname'),
            'phone' => $request->input('phone'),
            'sex' => $request->input('sex'),
            'avatar' => $input['avatar']
        ]);
        return redirect()->route('user.show', $user->id);
    }

    public function show($id){
        if(app('router')->getRoutes()->match(app('request')->create(url()->previous()))->getName() == route('stepThree') && !empty($id)){
            $user = User::findOrFail($id);
            return view('third.show',compact('user'));
        } else {
            abort(404);
        }
    }

    public function postExpire(Request $request){
        if($request->ajax()) {
            $user =User::findOrFail(2);
            $rand = rand(1,1000);
            $user->update([
               'experience' => $rand
            ]);
            return response()->json($rand);
        }
    }
}

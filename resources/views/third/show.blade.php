@extends('layout.layout')
@section('content')
    {!! Form::open(array('class' => 'form-horizontal')) !!}
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Никнейм:</strong>
            {!! Form::text('nick', $user->nick, array('class' => 'form-control', 'placeholder' => 'Введите никнейм','disabled')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Имя:</strong>
            {!! Form::text('name', $user->name, array('class' => 'form-control', 'placeholder' => 'Ваше имя','disabled')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Имя:</strong>
            {!! Form::text('surname', $user->surname, array('class' => 'form-control', 'placeholder' => 'Ваша фамилия','disabled')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Аватар:</strong>
            @if($user->avatar)
                <img src="{!! public_path('image/').$user->avatar !!}" width="200px" height="200px" alt="{!! $user->name !!}" />
                @else
                <p>Автар не был загружен</p>
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Телефон:</strong>
            {!! Form::text('phone', $user->phone, array('class' => 'form-control', 'placeholder' => 'Ваш телефон','disabled')) !!}
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Пол:</strong>
            {!! Form::radio('sex', 'Мужской' , $user->sex == 'Мужской' ? true : false, array('disabled')) !!} <span>Мужской</span>
            {!! Form::radio('sex', 'Женский' , $user->sex == 'Женский' ? true : false, array('disabled')) !!} <span>Женский</span>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
            <strong>Соглашение о рассылке:</strong>
            <label class="btn btn-default">
                {!! Form::checkbox('success', true, true, array('class' => 'agree','disabled')) !!}
            </label>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 text-center">
        <button type="submit" class="btn btn-primary">Создать</button>
    </div>
    {!! Form::close() !!}
@endsection